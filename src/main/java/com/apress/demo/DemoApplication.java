package com.apress.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

  private static Logger log = LoggerFactory.getLogger(DemoApplication.class);

	public static void main(String[] args) {
    SpringApplication.run(DemoApplication.class, args);
	}

  @Value("${server.ip}")
  String serverIp;

  @Bean
  CommandLineRunner values(){
    return args -> {
      log.info(" > The Server IP is: " + serverIp); 
    };
  }
}
